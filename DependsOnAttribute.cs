using System;

namespace PluginSystem
{
    /// <summary>
    /// Определяет атрибут зависимости для классов плагинов.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class DependsOnAttribute : Attribute
    {
        public string Dependency { get; }

        public DependsOnAttribute(string dependency)
        {
            Dependency = dependency;
        }
    }
}

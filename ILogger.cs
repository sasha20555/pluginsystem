using System;

namespace PluginSystem
{
    /// <summary>
    /// Предоставляет функциональность для логирования информации различных уровней.
    /// </summary>
    public interface ILogger
    {
        void LogInfo(string message);
        void LogWarning(string message);
        void LogError(string message);
    }

    /// <summary>
    /// Реализация логгера, выводящего информацию в консоль.
    /// </summary>
    public class ConsoleLogger : ILogger
    {
        public void LogInfo(string message) => Console.WriteLine($"[INFO]: {message}");
        public void LogWarning(string message) => Console.WriteLine($"[WARNING]: {message}");
        public void LogError(string message) => Console.WriteLine($"[ERROR]: {message}");
    }
}

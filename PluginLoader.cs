using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;

namespace PluginSystem
{
    /// <summary>
    /// Загрузчик плагинов, управляющий регистрацией и выполнением плагинов.
    /// </summary>
    public class PluginLoader
    {
        private readonly Dictionary<string, Plugin> _plugins = new();
        private readonly Dictionary<string, AssemblyLoadContext> _pluginContexts = new();
        private readonly Dictionary<string, List<string>> _dependencies = new();
        private readonly ILogger _logger;

        public PluginLoader(ILogger logger)
        {
            _logger = logger;
        }

        public void RegisterPlugin(string pluginPath)
        {
            try
            {
                _logger.LogInfo($"Попытка загрузки плагина из {pluginPath}");
                var fullPath = Path.GetFullPath(pluginPath ?? throw new ArgumentNullException(nameof(pluginPath)));
                var loadContext = new AssemblyLoadContext(fullPath, true);
                var assembly = loadContext.LoadFromAssemblyPath(fullPath);
                var pluginTypes = assembly.GetTypes().Where(t => typeof(IPlugin).IsAssignableFrom(t) && !t.IsInterface && !t.IsAbstract).ToList();

                if (pluginTypes.Count > 0)
                {
                    foreach (var pluginType in pluginTypes)
                    {
                        if (Activator.CreateInstance(pluginType) is IPlugin pluginInstance)
                        {
                            var dependencies = pluginType.GetCustomAttributes<DependsOnAttribute>().Select(attr => attr.Dependency).ToList();
                            var plugin = new Plugin(pluginInstance.Name, pluginInstance.Version, dependencies);

                            _plugins[plugin.Name] = plugin;
                            _pluginContexts[plugin.Name] = loadContext;
                            _dependencies[plugin.Name] = dependencies;
                            _logger.LogInfo($"Успешно загружен плагин {plugin.Name} версии {plugin.Version}");
                        }
                    }
                }
                else
                {
                    _logger.LogWarning($"В {fullPath} не найдены допустимые типы плагинов");
                    loadContext.Unload();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Ошибка загрузки плагина из пути {pluginPath}: {ex.Message}");
            }
        }


        public void LoadPlugins(string pluginDirectory)
        {
            foreach (var pluginFile in Directory.GetFiles(pluginDirectory, "*.dll"))
            {
                RegisterPlugin(pluginFile);
            }

            var sortedPlugins = TopologicalSort();
            foreach (var pluginName in sortedPlugins)
            {
                ExecutePlugin(pluginName);
            }
        }

        private void ExecutePlugin(string pluginName)
        {
            try
            {
                if (_pluginContexts.TryGetValue(pluginName, out var context) && context != null)
                {
                    var assembly = context.Assemblies.First();
                    var pluginType = assembly.GetTypes().FirstOrDefault(t => typeof(IPlugin).IsAssignableFrom(t) && !t.IsInterface);
                    if (pluginType != null && Activator.CreateInstance(pluginType) is IPlugin plugin)
                    {
                        plugin.Execute();
                        _logger.LogInfo($"Выполнен плагин {plugin.Name}");
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Ошибка выполнения плагина {pluginName}: {ex.Message}");
            }
        }

        private List<string> TopologicalSort()
        {
            var inDegree = new Dictionary<string, int>();
            foreach (var plugin in _plugins.Values)
            {
                if (!inDegree.ContainsKey(plugin.Name))
                {
                    inDegree[plugin.Name] = 0;
                }

                foreach (var dependency in _dependencies[plugin.Name])
                {
                    if (!inDegree.ContainsKey(dependency))
                    {
                        inDegree[dependency] = 0;
                    }
                    inDegree[dependency]++;
                }
            }

            var queue = new Queue<string>(_plugins.Keys.Where(k => inDegree[k] == 0));
            var sorted = new List<string>();

            while (queue.Count > 0)
            {
                var node = queue.Dequeue();
                sorted.Add(node);

                if (_dependencies.ContainsKey(node))
                {
                    foreach (var n in _dependencies[node])
                    {
                        inDegree[n]--;
                        if (inDegree[n] == 0)
                        {
                            queue.Enqueue(n);
                        }
                    }
                }
            }

            if (sorted.Count != _plugins.Count)
            {
                _logger.LogError("Обнаружена циклическая зависимость между плагинами");
                throw new InvalidOperationException("Обнаружена циклическая зависимость");
            }

            return sorted;
        }
    }
}

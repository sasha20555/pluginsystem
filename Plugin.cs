using System;
using System.Collections.Generic;

namespace PluginSystem
{
    /// <summary>
    /// Класс, представляющий плагин с именем, версией и зависимостями.
    /// </summary>
    public class Plugin
    {
        public readonly string Name;
        public readonly Version Version;
        public readonly IEnumerable<string> Dependencies;

        public Plugin(string name, Version version, IEnumerable<string> dependencies)
        {
            Name = name;
            Version = version;
            Dependencies = dependencies ?? new List<string>();
        }
    }
}

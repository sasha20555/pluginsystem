using System;

namespace PluginSystem
{
    /// <summary>
    /// Представляет интерфейс плагина с именем и версией, который можно выполнить.
    /// </summary>
    public interface IPlugin
    {
        string Name { get; }
        Version Version { get; }
        void Execute();
    }
}

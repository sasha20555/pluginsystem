using System;
using System.IO;

namespace PluginSystem
{
    /// <summary>
    /// Главный класс программы для запуска загрузчика плагинов.
    /// </summary>
    class Program
    {
        static void Main()
        {
            var pluginDirectory = Path.Combine(Directory.GetCurrentDirectory(), "Plugins");
            var logger = new ConsoleLogger();
            var pluginLoader = new PluginLoader(logger);

            pluginLoader.LoadPlugins(pluginDirectory);
            Console.WriteLine("Нажмите любую клавишу для выхода...");
            Console.ReadKey();
        }
    }
}
